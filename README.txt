Description
===========

Shoprocket module integrates Shoprocket with your Drupal site. The configuration
 of purchaseable products is done through Shoprocket dashboard but the regular
content is all controlled by Drupal.

Requirements
============

Shoprocket requires jQuery 1.10 therefore jQuery Update is a requirement and
using the 1.10 version of jQuery.

Installation
============

1. Download and install the jQuery Update module as usual
2. Update your jQuery version to at least 1.10 via the jQuery Update module
   settings page located at admin/config/development/jquery_update.
3. Download and install the Shoprocket module as usual
4. Navigate to block admin page located at admin/structure/blocks
5. Configure the "Shoprocket Cart" block, positioning it somewhere on a page and
   entering your cart id from Shoprocket.
